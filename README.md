Ruby on Rails- Advanced Features Application. 

This application contains a range of ruby on rails features.

Data Validation: validations are added to toy app features in Users and Micro-posts pages. Further validations are added to login and signup pages

Styles and Layouts: Bootstrap is integrated to the application. Further custom css are added manually

Authentication: Authentication is added to Login and it displays hidden pages when the user logs in

Internationalization : Internationalization is added to the home page. Items in the jumbotoron can be translated into French and German.

Agile Test Driven Design : Variety of tests were created during the development process. They can be accessed through the test folder

Extra Features : User search item/ Internationalization