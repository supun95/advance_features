require 'test_helper'

class MemberTest < ActiveSupport::TestCase

  def setup
    @member = Member.new(name: "Example User", email: "user@example.com")
  end

  test "should be valid" do
    assert @member.valid?
  end
  
  test "name should be present" do
    @member.name = "     "
    assert_not @member.valid?
  end
  
  test "email should be present" do
    @member.email = "     "
    assert_not @member.valid?
  end
  
  test "name should not be too long" do
    @member.name = "a" * 51
    assert_not @member.valid?
  end

  test "email should not be too long" do
    @member.email = "a" * 244 + "@example.com"
    assert_not @member.valid?
  end
end